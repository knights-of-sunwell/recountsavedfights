if not Recount then return end

local Recount = Recount
local me={}

local RecountSavedFights = LibStub("AceAddon-3.0"):NewAddon("RecountSavedFights", "AceEvent-3.0", "AceTimer-3.0","AceConsole-3.0")
RecountSavedFights.Version = tonumber(string.sub("$Revision: 1 $", 12, -3))

local RL = LibStub("AceLocale-3.0"):GetLocale("Recount")
local L = LibStub("AceLocale-3.0"):GetLocale("RecountSavedFights")

local function deepcopy(object)
	local lookup_table = {}
	local function _copy(object)
		if type(object) ~= "table" then
			return object
		elseif lookup_table[object] then
			return lookup_table[object]
		end
		local new_table = {}
		lookup_table[object] = new_table
		for index, value in pairs(object) do
			new_table[_copy(index)] = _copy(value)
		end
		return setmetatable(new_table, getmetatable(object))
	end
	return _copy(object)
end

function RecountSavedFights:OnInitialize()
    RecountSFDB = RecountSFDB or {}
    RecountSavedFights.db = RecountSFDB
    
    RecountSavedFights.db.combatants = RecountSavedFights.db.combatants or {}
	RecountSavedFights.db.CombatTimes = RecountSavedFights.db.CombatTimes or {}
	RecountSavedFights.db.FoughtWho = RecountSavedFights.db.FoughtWho or {}
    
    
    local theFrame=Recount.MainWindow
    theFrame.SaveButton=CreateFrame("Button",nil,theFrame)
	theFrame.SaveButton:SetNormalTexture("Interface\\Addons\\\RecountSavedFights\\Textures\\icon-save")
	theFrame.SaveButton:SetHighlightTexture("Interface\\Buttons\\UI-Panel-MinimizeButton-Highlight.blp")
	theFrame.SaveButton:SetWidth(16)
	theFrame.SaveButton:SetHeight(16)
	theFrame.SaveButton:SetPoint("RIGHT",theFrame.ReportButton,"LEFT",-3,0)
	theFrame.SaveButton:SetScript("OnClick",function(self) 
                        RecountSavedFights:SFDropDownOpen(self)
                        ToggleDropDownMenu(1, nil, RSF_DropDownMenu) 
                        end)
	theFrame.SaveButton:SetFrameLevel(theFrame.SaveButton:GetFrameLevel()+1)
    
    Recount:SetupMainWindowButtons()
end

function me:UpdateDetailData()
	if Recount.DetailWindow:IsVisible() and Recount.MainWindow.Selected then
		local _, Data=Recount.MainWindow:GetData(Recount.db2.combatants[Recount.MainWindow.Selected])
		local mode=Recount.DetailMode

		if type(Data)=="table" then
			if type(Data[mode][2])~="function" then
				Recount:SetupDetailTitles(Recount.MainWindow.Selected,Data[mode][2],Data[mode][3])
				Recount:FillUpperDetailTable(Data[mode][1])
			else
				Data[mode][2](Recount,Recount.MainWindow.Selected,Data[mode][1])
			end
		end
	end
end

function RecountSavedFights:SFDropDownOpen(myframe)
	RSF_DropDownMenu = CreateFrame("Frame", "RSF_DropDownMenu", myframe);
	RSF_DropDownMenu.displayMode = "MENU";
	RSF_DropDownMenu.initialize	= me.CreateFSDropdown;
	local leftPos = myframe:GetLeft()
	local rightPos = myframe:GetRight()
	local side
	local oside
	if not rightPos then
		rightPos = 0
	end
	if not leftPos then
		leftPos = 0
	end

	local rightDist = GetScreenWidth() - rightPos

	if leftPos and rightDist < leftPos then
		side = "TOPLEFT"
		oside = "TOPRIGHT"
	else
		side = "TOPRIGHT"
		oside = "TOPLEFT"
	end

	UIDropDownMenu_SetAnchor(RSF_DropDownMenu , 0, 0, oside, myframe, side)
end


function me:CheckIfSaved(numCurFight)
    for i,v in ipairs(RecountSavedFights.db.FoughtWho) do
        if v == Recount.db2.FoughtWho[numCurFight] then 
            return i
        end
    end
    return nil
end

function me:CheckIfPresent(k)
    for i,v in ipairs(Recount.db2.FoughtWho) do
        if v == RecountSavedFights.db.FoughtWho[k] then 
            return i
        end
    end
    return nil
end

function me:SaveFight(numCurFight)
    local numNewSF = #RecountSavedFights.db.FoughtWho + 1
    for k,v in pairs(Recount.db2.combatants) do
        if v.Fights[Recount.db.profile.CurDataSet] then
            if RecountSavedFights.db.combatants[k] then
                RecountSavedFights.db.combatants[k].Fights["Fight" .. numNewSF] = deepcopy(v.Fights["Fight" .. numCurFight])
            else
                RecountSavedFights.db.combatants[k] = deepcopy(v)
                RecountSavedFights.db.combatants[k].Fights["Fight" .. (Recount.db.profile.MaxFights + 1)] = nil
            end
            RecountSavedFights.db.FoughtWho[numNewSF] = Recount.db2.FoughtWho[numCurFight]
        end
    end
end

function me:DeleteFight()
    
end

function me:PickFight(k)
    local num = me:CheckIfPresent(k)
    if num then
        Recount.db.profile.CurDataSet = "Fight".. num
    else
      for i,v in pairs(Recount.db2.combatants) do
          v.Fights["Fight".. (Recount.db.profile.MaxFights + 1)] = nil
      end
      for i,v in pairs(RecountSavedFights.db.combatants) do
        if v.Fights["Fight"..k] then
            if not Recount.db2.combatants[i] then
                Recount.db2.combatants[i] = deepcopy(v)
                Recount.db2.combatants[i].Fights["Fight"..k] = nil
            end
            Recount.db2.combatants[i].Fights["Fight".. (Recount.db.profile.MaxFights + 1)] = v.Fights["Fight"..k]
        end
      end
      Recount.db2.FoughtWho[Recount.db.profile.MaxFights + 1] = RecountSavedFights.db.FoughtWho[k]
      Recount.db.profile.CurDataSet = "Fight".. (Recount.db.profile.MaxFights + 1)
    end
    me:UpdateDetailData()
    Recount.MainWindow.DispTableSorted={}
    Recount.MainWindow.DispTableLookup={}
    Recount.FightName = RecountSavedFights.db.FoughtWho[k]
    Recount:RefreshMainWindow()
    ToggleDropDownMenu(1, nil, RSF_DropDownMenu)
end


function me:CreateFSDropdown(level)
    local info = {}
    local numCurFight
        info.isTitle = 1
        info.text = L["Saved Fights"]
        info.notCheckable = 1
        UIDropDownMenu_AddButton(info, level)
        table.wipe(info)
        
        if Recount.db.profile.CurDataSet:sub(1,5) == "Fight" then
            numCurFight = tonumber(Recount.db.profile.CurDataSet:sub(6))
            local isAlrSaved = me:CheckIfSaved(numCurFight)
            info.notCheckable = 1
            info.arg1 = numCurFight
            info.text = isAlrSaved and L["Delete Fight"] or L["Save Fight"]
            info.func = isAlrSaved and me.DeleteFight  or me.SaveFight
            UIDropDownMenu_AddButton(info, level)
            table.wipe(info)
        end
        		       
		for k, v in pairs(RecountSavedFights.db.FoughtWho) do
			info.checked = (numCurFight and k == me:CheckIfSaved(numCurFight)) and 1 or nil
			info.text = L["Fight"].." "..k.." - "..v
            info.arg1 = k
			info.func = me.PickFight
			UIDropDownMenu_AddButton(info, level)
		end
end
