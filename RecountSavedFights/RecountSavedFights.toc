﻿## Interface: 30300
## Title: RecountSavedFights
## Notes: Module for saving fights in Recount
## Author: Ritual
## OptionalDeps: Ace3, Recount
## SavedVariables: RecountSFDB
## X-Category: Combat
## X-Embeds: Ace3

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@

locales\RecountSavedFights-ruRU.lua


RecountSavedFights.lua
