-- RecountGuessedAbsorbs Locale 
-- Please use the Localization App on WoWAce to Update this 
-- http://www.wowace.com/projects/recountguessedabsorbs/localization/
 
local L = LibStub("AceLocale-3.0"):NewLocale("RecountSavedFights", "ruRU") 
if not L then return end 
 
L["Saved Fights"] = "Сохраненные бои"
L["Save Fight"] = "Сохранить бой"
L["Delete Fight"] = "Удалить бой"
L["Fight"] = "Бой"

